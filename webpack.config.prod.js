const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const MinifyPlugin = require("babel-minify-webpack-plugin");
const path = require('path');

module.exports = function (env, argv) {
	return {
		mode: 'production',
		entry: [
			'./src/js/app.js'
		],
		output: {
			path: path.resolve(__dirname, 'public'),
			filename: '[name].bundle.js',
		},
		optimization: {
			minimizer: [
				new OptimizeCSSAssetsPlugin()
			]
		},
		plugins: [
			new CleanWebpackPlugin(['public']),
			new HtmlWebpackPlugin({
				filename: "index.html",
				template: path.resolve('./src/index.html'),
				minify: false
			}),
			new MiniCssExtractPlugin({
				filename: "[name].bundle.css",
				chunkFilename: "[id].css"
			}),
			new MinifyPlugin()
		],
		module: {
			rules: [
				{
					test: require.resolve('jquery'),
					loader: 'expose-loader',
					options: {
						exposes: ['$', 'jQuery'],
					},
				},
				{
					test: /\.scss$/,
					use: [
						MiniCssExtractPlugin.loader,
						{
							loader: 'css-loader',
							options: {
								sourceMap: true,
								modules: true,
								localIdentName: '[local]'
							}
						},
						{
							loader: 'postcss-loader',
							options: {
								sourceMap: true,
								config: {
									path: 'postcss.config.js'
								}
							}
						},
						{
							loader: 'sass-loader',
							options: {
								sourceMap: true
							}
						}
					]
				},
				{
					test: /\.m?js$/,
					exclude: /(node_modules|bower_components)/,
					use: {
						loader: 'babel-loader',
						options: {
							presets: ['@babel/preset-env']
						}
					}
				},
				{
					test: /\.(pdf|doc|docx|xls|xlsx|txt|csv|tsv)$/,
					use: [
						{
							loader: "file-loader",
							options: {
								outputPath: './files',
								name: "[name].[ext]",
							},
						}
					]
				},
				{
					test: /\.(jpg|jpeg|gif|png|webp|ico)$/,
					use: [
						{
							loader: "file-loader",
							options: {
								outputPath: './images',
								name: "[name].[ext]",
							},
						},
						{
							loader: 'image-webpack-loader',
							options: {
								mozjpeg: {
									progressive: false,
									quality: 90
								},
								// optipng.enabled: false will disable optipng
								optipng: {
									enabled: true,
								},
								pngquant: {
									quality: '90-100',
									speed: 4
								},
								gifsicle: {
									interlaced: true,
									optimizationLevel: 3
								},
								// the webp option will enable WEBP
								webp: {
									quality: 90
								}
							}
						},
					],
				},
				{
					test: /\.(woff(2)?|ttf|otf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
					use: [
						{
							loader: 'file-loader',
							options: {
								name: '[name].[ext]',
								outputPath: 'fonts/'
							}
						}
					]
				},
				{
					test: /\.html$/,
					use: {
						loader: 'html-loader?interpolate',
						options: {
							attrs: [':src', ':href'],
							interpolate: true
						}
					}
				},
			]
		}
	};
}
