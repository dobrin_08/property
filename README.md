## Webpack Starter Kit project

### Features:
 - Compilation of ES6 into ES5
 - Converting SASS into CSS
 - Bootstrap 4
 - Image loading and optimization
 - Webpack dev server
 - HMR for hot reloading and faster development
 
### Installation
1. Clone the project
2. Go to the project root directory
3. Run `yarn install`

### Running on development using [dev server](https://github.com/webpack/webpack-dev-server)

Run `yarn start` to start to webpack dev server with HMR ready

### For production 

Run `yarn build` to build project's all assets in `dist` folder.

----------------------
The project's initial version was created by [thecodeholic](https://github.com/thecodeholic/webpack-starter-kit)
