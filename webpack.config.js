const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const path = require('path');

module.exports = function () {
	return {
		mode: 'development',
		entry: [
			'./src/js/app.js'
		],
		watch: true,
		watchOptions: {
			aggregateTimeout: 300, // Process all changes which happened in this time into one rebuild
			poll: 1000, // Check for changes every second,
			ignored: /node_modules/,
		},
		devtool: 'source-maps',
		devServer: {
			contentBase: path.join(__dirname, 'src'),
			watchContentBase: true,
			hot: true,
			open: true,
			inline: true
		},
		plugins: [
			new HtmlWebpackPlugin({
				filename: "index.html",
				template: path.resolve('./src/index.html'),
				minify: false
			}),
			new webpack.HotModuleReplacementPlugin(),
			new webpack.ProvidePlugin({
				$: "jquery",
				jQuery: "jquery"
			})
		],
		module: {
			rules: [
				{
					test: /\.scss$/,
					use: [
						'style-loader',
						"css-loader",
						"sass-loader"
					]
				},
				{
					test: /\.m?js$/,
					exclude: /(node_modules|bower_components)/,
					use: {
						loader: 'babel-loader',
						options: {
							presets: ['@babel/preset-env']
						}
					}
				},
				{
					test: /\.(pdf|doc|docx|xls|xlsx|txt|csv|tsv)$/,
					use: [
						{
							loader: "file-loader",
							options: {
								outputPath: './files',
								name: "[name].[ext]",
							},
						}
					]
				},
				{
					test: /\.(jpg|jpeg|gif|png|webp|ico)$/,
					use: [
						{
							loader: "file-loader",
							options: {
								outputPath: './images',
								name: "[name].[ext]",
							},
						},
					]
				},
				{
					test: /\.(woff(2)?|ttf|otf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
					use: [
						{
							loader: 'file-loader',
							options: {
								name: '[name].[ext]',
								outputPath: 'fonts/'
							}
						}
					]
				},
				{
					test: /\.html$/,
					use: {
						loader: 'html-loader?interpolate',
						options: {
							attrs: [':src', ':href'],
							interpolate: true
						}
					}
				},
			]
		}
	};
}
