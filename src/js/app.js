import '../scss/style.scss';

import $ from 'jquery';
import 'bootstrap';
import 'popper.js/dist/popper.min';

// Cards toggle info text
$('.card.agent .fa-info-circle').on('click', function () {
	$(this)
		.toggleClass('fa-times')
		.parents('.card-img-overlay')
		.find('.card-img-overlay-text')
		.toggleClass('visible');
});

// Scroll to top button
let btn = $('#scroll-top');

$(window).scroll(function () {
	if ($(window).scrollTop() > 500) {
		btn.addClass('show');
	} else {
		btn.removeClass('show');
	}
});

btn.on('click', function (e) {
	e.preventDefault();
	$('html, body').animate({scrollTop: 0}, '300');
});
